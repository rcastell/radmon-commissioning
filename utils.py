import os
import smtplib
from email.message import EmailMessage

import pandas as pd

from settings import *


def read_mappings(mappings_folder):
    """
    :param mappings_folder: the path where the mappings files are stored
    :return: a pandas Dataframe containing all the mapping data.
    """
    mappings_df=pd.DataFrame(columns=['serial','timbername','fipaddr','ID'])
    for mapping_name in mappings_files:
        mydf=pd.read_csv(os.path.join(mappings_folder,mapping_name),skiprows=1,
                     names=['serial','timbername','fipaddr','ID'],index_col='timbername') 
        mappings_df = mappings_df.append(mydf)
    return mappings_df

def instance_to_timber(instance_name):
    """
    converts RADMON.3RM.10R3-20 to SIMA.10R3.3RM20S
    
    :param instance_name: the name of the radmon in ccdb
    :return: the name of the radmon in timber
    """
    if "NA62" in instance_name:
        number=instance_name.split('-')[1]
        return 'SIMA.'+number
    if "PSB" in instance_name:
        number=instance_name.split('-')[1]
        return 'BR.SIMA-'+number
    if "PS" in instance_name and ("U" not in instance_name):
        number=instance_name.split('-')[1]
        return 'PR-SIMA-'+number
    if "I0" in instance_name:
        second_part= instance_name.split('.')[1]
        return 'SIMA.'+second_part
    if "ALICE" in instance_name:
        number=instance_name.split('-')[1]
        return 'SIMA.ALICE'+number
    if "CHARM" in instance_name:
        number=instance_name.split('-')[1]
        return 'SIMA.CHARMB'+number        
    fip_address=instance_name.split('-')[1]
    fec=instance_name.split('.')[1]
    cell_string=instance_name.split('.')[2]
    if 'L' in cell_string:
        cell = cell_string.partition('L')[0]
        point = cell_string.partition('L')[2][0]
        side='L'
    if 'R' in cell_string:
        cell=cell_string.partition('R')[0]
        point = cell_string.partition('R')[2][0]
        side='R'  
    if 'USC' in instance_name:
        usc_position= cell_string[3:5]
        return 'SIMA.USC'+usc_position+'.'+fec+fip_address.zfill(2)+'S'
    if 'UA' in instance_name:
        ua_position= cell_string[2:4]
        return 'SIMA.UA'+ua_position+'.'+fec+fip_address.zfill(2)+'S'
    if 'UPS' in instance_name:
        ups_position= cell_string[3:5]
        return 'SIMA.UPS'+ups_position+'.'+fec+fip_address.zfill(2)+'S'
    if "RR" in instance_name:
        rr_position=instance_name.split('RR')[1][:2]
        return 'SIMA.RR'+rr_position+'.'+fec+fip_address.zfill(2)+'S'
    if "RE" in instance_name:
        re_position=instance_name.split('RE')[1][:2]
        return 'SIMA.RE'+re_position+'.'+fec+fip_address.zfill(2)+'S'
    if "TZ" in instance_name:
        tz_position=instance_name.split('TZ')[1][:2]
        return 'SIMA.TZ'+tz_position+'.'+fec+fip_address.zfill(2)+'S'
    if "UJ" in instance_name:
        uj_position=instance_name.split('UJ')[1][:2]
        return 'SIMA.UJ'+uj_position+'.'+fec+fip_address.zfill(2)+'S'
    return 'SIMA.'+cell+side+point+'.'+fec+fip_address.zfill(2)+'S'
    
def timber_to_instance(timber_name):
    """
    converts SIMA.10R3.3RM20S:varname to RADMON.3RM.10R3-20 
    
    :param instancename: the name of the radmon in timber
    :return: the name of the radmon in ccdb
    """
    if len(timber_name.split(':')[0]) <= 8:
        print(timber_name,"RADMON.NA62-"+timber_name.split(".")[1])
        return "RADMON.NA62-"+timber_name.split(".")[1]
    if "BR" in timber_name:
        "radmon in PSB"
        number=timber_name.split('-')[1]
        return "RADMON.PSB-"+number
    if "PR" in timber_name:
        "radmon in PS"
        number=timber_name.split('A')[1]
        return "RADMON.PS"+number
    if "I0" in timber_name:
        "radmon in SPS"
        second_part = timber_name.split('.')[1]
        return "RADMON."+second_part
    if "ALICE" in timber_name:
        number=timber_name.split('.')[1][5:]
        return 'RADMON.ALICE-'+number
    if "CHARM" in timber_name:
        number=timber_name.split('.')[1][6:]
        return 'RADMON.CHARMB-'+number    
    timber_name=timber_name.split(':')[0]
    if "ALICE" in timber_name:
        number=timber_name.split('.')[1][5:]
        return 'RADMON.ALICE-'+number
    if "CHARM" in timber_name:
        number=timber_name.split('.')[1][6:]
        return 'RADMON.CHARMB-'+number 
    try:
        fip_address=int(timber_name[-3:-1])
    except:
        print('except',timber_name)
    cell_string=timber_name.split('.')[1]
    fec=timber_name.split('.')[2][:3]
    if 'USC' in timber_name:
        return 'RADMON.'+fec+'.'+cell_string+'-'+str(fip_address)
    if 'UA' in timber_name:
        return 'RADMON.'+fec+'.'+cell_string+'-'+str(fip_address)
    if 'UPS' in timber_name:
        return 'RADMON.'+fec+'.'+cell_string+'-'+str(fip_address)
    if 'TZ' in cell_string:
        return 'RADMON.'+fec+'.'+cell_string+'-'+str(fip_address)
    if 'UJ' in cell_string:
        return 'RADMON.'+fec+'.'+cell_string+'-'+str(fip_address)
    if 'L' in cell_string:
        cell = cell_string.partition('L')[0]
        point = cell_string.partition('L')[2]
        side='L'
    if 'R' in cell_string:
        cell=cell_string.partition('R')[0]
        point = cell_string.partition('R')[2]
        side='R'    
    return 'RADMON.'+fec+'.'+cell+side+point+'-'+str(fip_address)

def send_mail(recipient,content,subject='Exception in script'):
    """
    sends an email when an exception occurs
    
    :param recipient: the recepient of the message
    :param content: the content of the message
    """
    msg = EmailMessage()
    msg.set_content(content)
    msg['Subject'] = subject
    msg['From'] = 'radmon_script'
    msg['To'] = recipient
    
    # Send the message via our own SMTP server.
    s = smtplib.SMTP('localhost')
    s.send_message(msg)
    s.quit()
    
#put proper tests
if __name__=="__main__":
    timber  = 'SIMA.10R3.3RM20S'
    ccdb    = 'RADMON.3RM.10R3-20'
    assert(instance_to_timber(ccdb)==timber)
    assert(timber_to_instance(timber)==ccdb)
