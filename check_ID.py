import os
import xml.etree.ElementTree as ElementTree

import pandas as pd

from settings import *
from utils import read_mappings


def radmonID_from_instance(instance_name):
    """
    :return: a dict with keys the radmons in the instance files and value the ID of the Radmon
    """
    root = ElementTree.parse('resources/Instances/DeviceData_RadmonV6_DU_cfc-sr2-sr2b.instance').getroot()
    dict_radmons_instance={}
    for (radmon,id) in zip(root.iter('device-instance'),root.iter('Identification')):
        dict_radmons_instance[radmon.attrib["name"]]=list(id.iter('value'))[0].text
#    for radmon in instances_dict[instance_name]['instantiation-unit']['classes']['RadmonV6']['device-instance']:
#        dict_radmons_instance[radmon['@name']]=radmon['configuration']['Identification']['value']
    return dict_radmons_instance                

if __name__ == '__main__':
    mappings=read_mappings(MAPPINGS_FOLDER)
    all_fecs=os.listdir(INSTANCES_FOLDER)
    dictID_instances={}
    for instance in all_fecs:
        dictID_instances.update(radmonID_from_instance(instance))
    i=0    
    for key,val in dictID_instances.items():
        if not (mappings.loc[key,'ID']==int(val)):
            i=1
            print('radmon {} with ID {} in instance has ID {} in mapping'
                  .format(key,val,mappings.loc[key,'ID']))
    
    if (i==0):
        print('all RadMon IDs match')
