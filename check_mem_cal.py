#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 26 17:37:04 2018

@author: rcastell
"""
"""
NOTE: analysis of 7.4.1 instance: 
caltable memorybank1 {{HEH_3.3V},{TH_2.5V}}
caltable memorybank2 {HEH_5V,TH_5V,HEH_3V,TH_3V}
MBUs are not taken into account

"""
import pyjapc
import xml.etree.ElementTree
from datetime import datetime
import time
import numpy as np
import pandas as pd
import sys

instances=[#'/acc/dsc/lhc/cfc-se5-sl5a/data/RadmonV6_DU.cfc-se5-sl5a.instance',
#            '/acc/dsc/lhc/cfc-se5-sr5b/data/RadmonV6_DU.cfc-se5-sr5b.instance',
#            '/acc/dsc/lhc/cfc-sr1-sl1a/data/RadMon_DeployUnit.cfc-sr1-sl1a.instance',
#           '/acc/dsc/lhc/cfc-sr1-sr1b/data/RadmonV6_DU.cfc-sr1-sr1b.instance',
#            '/acc/dsc/lhc/cfc-sr2-sr2b/data/RadmonV6_DU.cfc-sr2-sr2b.instance',
#             '/acc/dsc/lhc/cfc-sr2-sl2a/data/RadmonV6_DU.cfc-sr2-sl2a.instance',
#             '/acc/dsc/lhc/cfc-sr8-sl8a/data/RadmonV6_DU.cfc-sr8-sl8a.instance',
#             '/acc/dsc/lhc/cfc-sr8-sr8b/data/RadmonV6_DU.cfc-sr8-sr8b.instance'
             ]

#these radmons are not working for some reason
not_working_radmons = ['RADMON.1LM.UJ13-3','RADMON.1LM.UJ14-1','RADMON.2LM.UA23-3']

def myexc(p,d,e):
    print(p,d,e)

def get_subscriptions(devices,seconds_subscriptions):
    """
    :param devices: a list of the radmons to scan
    :return: a dictionary with the scanned radmon as keys and the 
                delay in milliseconds between two published data
    """
    d={}
    voltages={}
    for radmon in devices:
        if radmon in not_working_radmons:
            continue
        for item in list_subscribe:
            d[radmon]={}
            d[radmon][item]=0

    def update_dict(paramName, newVal):
            d[paramName.split('/')[0]][paramName.split('/')[1]] = newVal
    
    #read the voltages of the memories
    for radmon in devices:
        voltages[radmon]={}
        if radmon in not_working_radmons:
            continue
        voltages[radmon]['Toshiba']=japc.getParam(radmon+'/ExpertSettings#ToshibaBankVoltage')
        voltages[radmon]['Cypress']=japc.getParam(radmon+'/ExpertSettings#CypressBankVoltage')

    #start SEU random generation
    for radmon in devices:
        if radmon in not_working_radmons:
            continue
        for item in list_set:
            japc.setParam(radmon+'/'+item,True)

    #subscriptions
    for radmon in devices:
        if radmon in not_working_radmons:
            continue
        for item in list_subscribe:
            japc.subscribeParam(radmon+'/'+item,update_dict,
                                onException=myexc)
            
    japc.startSubscriptions()
    time.sleep(seconds_subscriptions)
    japc.stopSubscriptions()
    japc.clearSubscriptions()
    
    #start SEU random generation
    for radmon in devices:
        if radmon in not_working_radmons:
            continue
        for item in list_set:
            japc.setParam(radmon+'/'+item,False)

    return d, voltages


def get_radmons(instance):
    """
    :return: list of radmons from instance
    """
    root = xml.etree.ElementTree.parse(instance).getroot()
    devicenames = []
    timing = root.iter('timingDomain')
    timing = next(timing)
    timing= timing.get('value')
    print(timing)
    selector = timing + ".USER.ALL"
    japc.setSelector( selector)
    for devices in root.iter('device-instance'):
        devicenames.append(devices.get("name"))
    return devicenames   

def get_lot(meas_cal,cal_df,maker):
    """
    gets lot from the measured calibration factor used
    """
    
    if maker=='Toshiba':
        "4 chips, 4Mb each"
        meas_cal_normalized=meas_cal/(2**24)
    if maker == 'Cypress':
        "4 chips, 8Mb each"    
        meas_cal_normalized=meas_cal/(2**25)
    for col in cal_df:
        try:
            idx=df.index[abs(df[col]-meas_cal_normalized)<1e-15][0]
            return maker,idx,col
        except IndexError:
            pass
    
#this reads the calibration factors from a file
df=pd.read_csv('resources/cal_factors.csv',header=None,index_col=['lot'],
                        names=['maker','lot','heh5v','th5v','heh3v3','th3v3']).drop('maker', axis=1)

#set SEURandomGenerator and reset it at the end of the script
list_set=['ExpertCommands#SEURandomGenerator']

#get memories bank voltages
list_get=['ExpertSettings#CypressBankVoltage','ExpertSettings#ToshibaBankVoltage']

#subscribe to counts and fluence to see which calibration factor
#is used by the FESA class to calculate the Hadron fluence
list_subscribe=['Acquisition#cumBank2SEUCounts', 'Acquisition#cumBank1SEUCounts',
          'Acquisition#cumBank1HadronFluence','Acquisition#cumBank2HadronFluence']

if __name__ == '__main__':
    japc = pyjapc.PyJapc(noSet=False)
    japc.rbacLogin()
    instance_path='/acc/dsc/lhc/{0}/data/RadmonV6_DU.{0}.instance'.format(sys.argv[1])
    radmons_list=get_radmons(instance_path)
    japc_dict,voltages=get_subscriptions(radmons_list,1)
    
    cal_factors={}
    for radmon in japc_dict.keys():
        cal_factors[radmon]={}
        cal_factors[radmon]['Toshiba']=\
                   japc_dict[radmon]['Acquisition#cumBank2SEUCounts']/japc_dict[radmon]['Acquisition#cumBank2HadronFluence']
        cal_factors[radmon]['Cypress']=\
                   japc_dict[radmon]['Acquisition#cumBank1SEUCounts']/japc_dict[radmon]['Acquisition#cumBank1HadronFluence']
        
    for radmon in japc_dict.keys():
        print(radmon,get_lot(cal_factors[radmon]['Toshiba'],df,'Toshiba'))
        print(radmon,get_lot(cal_factors[radmon]['Cypress'],df,'Cypress'))    
