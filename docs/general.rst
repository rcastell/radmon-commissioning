General info
=============

The JSON is published by ``v6alerts.py`` in the eos folder of the website.

To run the script you will need an AFS token in order to be able to store data in EOS. You can do so by running ``k5reauth -i 3600 -f /user/bdisoft/operational/bin/Python/anaconda_py36/bin/python v6alerts.py``.

In order to keep deployment easy, only packages in anaconda as provided in https://wikis.cern.ch/display/ST/BI+Python+installation+on+NFS are used (which is the reason for the long line in the command to be run). In this way, it should be possible to just pull the git repo and run the k5reauth command shown above.
In the future, we should have our own anaconda+cern specific packages distribution, as the one by the BE/BI group is not officially supported.

INSTALLATION
============

To install the software and be able to run the various scripts, the following steps need to be performed:

1. Get a CERN Centos machine on the TN
2. Log in with a user in the ``ecerad`` group in gitlab.cern.ch and in the ``cernbox-project-radmonit-writers`` ``cernbox-project-radmonit-readers`` (``sdanzeca`` e.g.).
3. get your kerberos ticket by issuing ``kinit``
4. run ``git clone https://:@gitlab.cern.ch:8443/ecerad/radmon-commissioning.git`` this downloads the scripts in a subfolder called radmon-commissioning
5. issue ``tmux`` to open a shell that persists after your logout
6. enter the ``radmon-commissioning`` folder
7. in this shell issue ``k5reauth -i 3600 -f /user/bdisoft/operational/bin/Python/anaconda_py36/bin/python v6alerts.py``: this starts the script with the default option

