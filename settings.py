import os

curdir=os.path.dirname(os.path.abspath(__file__))
INSTANCES_FOLDER=os.path.join(curdir,'resources','Instances')
MAPPINGS_FOLDER=os.path.join(curdir,'resources','Mapping')

instances_files=os.listdir(INSTANCES_FOLDER)
mappings_files=os.listdir(MAPPINGS_FOLDER)
