#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 26 17:37:04 2018

@author: rcastell
"""

import pyjapc
import xml.etree.ElementTree
import time
import numpy as np
import sys

instances=[#'/acc/dsc/lhc/cfc-se5-sl5a/data/RadmonV6_DU.cfc-se5-sl5a.instance',
#            '/acc/dsc/lhc/cfc-se5-sr5b/data/RadmonV6_DU.cfc-se5-sr5b.instance',
#            '/acc/dsc/lhc/cfc-sr1-sl1a/data/RadmonV6_DU.cfc-sr1-sl1a.instance',
#           '/acc/dsc/lhc/cfc-sr1-sr1b/data/RadmonV6_DU.cfc-sr1-sr1b.instance',
            '/acc/dsc/lhc/cfc-sr2-sr2b/data/RadmonV6_DU.cfc-sr2-sr2b.instance',
#             '/acc/dsc/lhc/cfc-sr2-sl2a/data/RadmonV6_DU.cfc-sr2-sl2a.instance',
#             '/acc/dsc/lhc/cfc-sr8-sl8a/data/RadmonV6_DU.cfc-sr8-sl8a.instance',
#             '/acc/dsc/lhc/cfc-sr8-sr8b/data/RadmonV6_DU.cfc-sr8-sr8b.instance'
             ]
def myexc(p,d,e):
    print(p,d,e)

def get_subscriptions(devices,seconds_subscriptions):
    """
    :param devices: a list of the radmons to scan
    :return: a dictionary with the scanned radmon as keys and the 
                delay in milliseconds between two published data
    """
    d={}
    for radmon in devices:
        for item in list_get:
           # if item=='ExpertSensorAcquisition#RADFETs':
            d[radmon+'/'+item]=np.zeros(2)
#            else:
#                d[radmon+'/'+item]=0

    def update_if_max(paramName, newVal):
        """
        this callback only uploads the value if it is higher than the previous
        in order to find the max value while recording
        """
        try:
            if d[paramName] < newVal:
                d[paramName]=newVal
        except Exception as e:
            if d[paramName][0] < newVal[0]:
                d[paramName][0]=newVal[0]
            if d[paramName][1] < newVal[1]:
                d[paramName][1]=newVal[1]
            
    for radmon in devices:
        if radmon in ['RADMON.1LM.UJ13-3','RADMON.1LM.UJ14-1','RADMON.2LM.UA23-3']:
            continue
        #checking only the radmonStatus for now
        for item in list_get:
            japc.subscribeParam(radmon+'/'+item,update_if_max,
                                onException=myexc)#,timingSelectorOvveride=time_domain+'.USER.ALL')
    japc.startSubscriptions()
    time.sleep(seconds_subscriptions)
    japc.stopSubscriptions()
    japc.clearSubscriptions()
    
    for key in d.keys():
        try:
            if key.endswith('#RADFETs'):
                japc.setParam(key.partition('/')[0]+'/ExpertSettings#Radfet1InitialThreshold', d[key][0]*1000)
                japc.setParam(key.partition('/')[0]+'/ExpertCommands#ResetTID1',True)
                japc.setParam(key.partition('/')[0]+'/ExpertSettings#Radfet2InitialThreshold', d[key][1]*1000)
                japc.setParam(key.partition('/')[0]+'/ExpertCommands#ResetTID2',True)

            if key.endswith('PinDiodes'):
                japc.setParam(key.partition('/')[0]+'/ExpertSettings#PinDiodeInitialThreshold', d[key][0]*1000)
                japc.setParam(key.partition('/')[0]+'/ExpertCommands#ResetNeutronFluence',True)

        except Exception as e:
            print(key,e)
            
    #is this wait necessary?
    time.sleep(1)
    for key in d.keys():
        try:
            if key.endswith('#RADFETs'):
                japc.setParam(key.partition('/')[0]+'/ExpertCommands#ResetTID1',False)
                japc.setParam(key.partition('/')[0]+'/ExpertCommands#ResetTID2',False)



            if key.endswith('PinDiodes'):
                japc.setParam(key.partition('/')[0]+'/ExpertCommands#ResetNeutronFluence',False)

        except Exception as e:
            print(key,e)

    return d

def get_radmons(instance):
    """
    :return: list of radmons from instance
    """
    root = xml.etree.ElementTree.parse(instance).getroot()
    devicenames = []
    timing = root.iter('timingDomain')
    timing = next(timing)
    timing= timing.get('value')
    print(timing)
    selector = timing + ".USER.ALL"
    japc.setSelector( selector)
    for devices in root.iter('device-instance'):
        devicenames.append(devices.get("name"))
        
    return devicenames   

if __name__=="__main__":
    if len(sys.argv)==1:
        print("""YOU HAVE NOT SPECIFIED ANY INSTANCE FROM COMMAND LINE, 
              THIS SCRIPT WILL RUN WITH %s AS INSTANCE!!! """ %(instances[0]))
        sure = input('ARE YOU SURE?? (Y/N)')
        print(sure)
        if str(sure).lower()=='y':
            print('going on')
            time.sleep(1)
        else:
            sys.exit()
    japc = pyjapc.PyJapc(incaAcceleratorName="LHC", noSet=False )  #safemode
    japc.rbacLogin()

    if len(sys.argv)>1: 
        print(sys.argv[1])
        radmons_list=get_radmons(sys.argv[1])
        list_get=['ExpertSensorAcquisition#RADFETs', 'ExpertSensorAcquisition#PinDiodes' ]
        mydic=get_subscriptions(radmons_list,15)
        
    if len(sys.argv)==1:
        radmons_list=get_radmons(instances[0])
        list_get=['ExpertSensorAcquisition#RADFETs', 'ExpertSensorAcquisition#PinDiodes' ]
        mydic=get_subscriptions(radmons_list,15)
