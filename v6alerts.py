import argparse
import configparser
import json
import subprocess
import time
import traceback
import xml.etree.ElementTree
from datetime import datetime
import re
from collections import defaultdict

import numpy as np
import pyjapc
import pytimber
import xlsxwriter 

import utils


def isinRange(value, minVal, maxVal, mexLow, mexHigh):
    if value<minVal:
        return 'LOW: ' + mexLow
    if value>=maxVal:
        return 'HIGH: ' + mexHigh
    else:
        return 'NONE'



def write_xlsx(scanresults,timestamp,new_alerts_message,filename):
    
    workbook = xlsxwriter.Workbook(filename)
    worksheet = workbook.add_worksheet()
    
    def format_warnings(high_low):
        if high_low=='L':
            return workbook.add_format({'bg_color': '#FFB7BD'})
        if high_low=='H':
            return workbook.add_format({'bg_color': '#FFF59B'})
    
    alerts=defaultdict(list)
    print(new_alerts_message)
    for mess in new_alerts_message:
        try:
            name,pos,alert = re.split('(-[0-9]+)',mess)
            radmon=name+pos
        except ValueError:
            try:
                name,pos,alert = re.split('([0-9]+S)',mess)
                radmon=name+pos
            except:
                print('not converted',mess)
                continue
        alerts[radmon].append(alert)
    print(alerts)
    row=0
    col=0
    worksheet.write(row,col,timestamp)
    row+=1
    cols = list(scanresults[list(scanresults.keys())[0]].keys())
    cols.remove('timing')
    worksheet.write_row(row,1,cols)
    row+=1
    for key,val in alerts.items():
        try:
            res=[key]
            for chiave,valore in scanresults[key.upper()].items():
                if chiave is not 'timing':
                    res.append(valore)
            worksheet.write(row,col,key)
            col+=1
            for valore in res[1:]:
                try:
                    if valore['warning'][0] in ['H','L']:
                        worksheet.write(row,col,float(valore['value']),format_warnings(valore['warning'][0]))
                    else:
                        worksheet.write(row,col,float(valore['value']))
                    col+=1    
                except Exception as e:
                    print(e,valore)
            col=0    
            row+=1
        except Exception as e:
            print(e)
    workbook.close()
                
def add_to_scanresults(scanresults,col_name,data_dict,lim_low, lim_high):
    for key in scanresults.keys():
        try:
            scanresults[key][col_name]={}
            scanresults[key][col_name]['value']=data_dict[key]
            if float(data_dict[key]) < lim_low :
                scanresults[key][col_name]['warning']='LOW'
            else:
                if float(data_dict[key]) > lim_high:
                    scanresults[key][col_name]['warning']='HIGH'
                else:
                    scanresults[key][col_name]['warning']='NONE'
        except KeyError:
            scanresults[key][col_name]={}
            scanresults[key][col_name]['value']='NONE'
            scanresults[key][col_name]['warning']='HIGH'
            
def get_monitoring_data(instances,config_file, ignore_instance_list, ignore_radmons_list):
    """
    :param instances: a list of paths to .instance files
    :param config_file: the .INI file where the variables to check and 
        the high and low limits are stored
    :return: a dictionary with the scanned radmon as keys and the 
                delay in milliseconds between two published data
    :return: a list containing all the variables read
    """

    scanned_vars=[]
    scanresults={}
    for instance in instances:
        if instance.lower() in ignore_instance_list:
            continue
        root = xml.etree.ElementTree.parse(instance).getroot()
        devicename = []
        timing = root.iter('timingDomain')
        timing = next(timing)
        timing= timing.get('value')
        selector = timing + ".USER.ALL"
        japc.setSelector( selector)
        for devices in root.iter('device-instance'):
            devicename = devices.get("name")
            if devicename.lower() in ignore_radmons_list:
                continue
            scanresults[devicename]={}
            for varname in config.sections():
                varnameccdb = config[varname]["variableccdb"]
                varnametimber = config[varname]["variableccdb"]
                minVal= float(config[varname]["min"])
                maxVal= float(config[varname]["max"])
                mexHigh = config[varname]["messagehigh"]
                mexLow = config[varname]["messagelow"]
                exclusionlist = config[varname]["exclusionlist"]
                japcvar = devicename + "/" + varnameccdb 
                scanned_vars.append(japcvar)
                varlist =[]
                try:
                    varlist = japc.getParam(japcvar)
                except Exception as e:
                    print(e,devicename)
                if varnameccdb in ['ExpertSettings#Radfet1Gain','ExpertSettings#Radfet2Gain']:
                    scanresults[devicename][varname]={}
                    scanresults[devicename][varname]['value']=str('% 6.2f' % varlist[0])
                    scanresults[devicename][varname]['warning'] = 'NONE'
                    continue
                #radfets and pindiodes are arrays, not single floats
                scanresults[devicename]['timing']=timing
                if isinstance(varlist,np.ndarray):
                    for i,val in enumerate(varlist):
                        scanresults[devicename][varname+str(i)]={}
                        scanresults[devicename][varname+str(i)]['value']=str('% 6.4f' % val)
                        scanresults[devicename][varname+str(i)]['warning']=isinRange(val, minVal, maxVal, mexLow, mexHigh)
                else:
                    scanresults[devicename][varname]={}
                    if varname == 'voltage_SensorAdcRef':
                        scanresults[devicename][varname]['value']=str('% 6.4f' % varlist)
                        scanresults[devicename][varname]['warning'] = isinRange(varlist, minVal, maxVal, mexLow, mexHigh)
                    else:
                        scanresults[devicename][varname]['value']=str('% 6.2f' % varlist)
                        scanresults[devicename][varname]['warning'] = isinRange(varlist, minVal, maxVal, mexLow, mexHigh)
    return scanresults, scanned_vars

def read_on_timber(devices):
    """
    :param devices: a list of the radmons to scan
    :return: a dictionary with the scanned radmon as keys and the 
                timestamp of last published sample on timber
    """
    not_found_names=[]
    timber_names=[]
    for dev in devices:
        try:
            timber_names.append(utils.instance_to_timber(dev)+':PT100')
        except:
            not_found_names.append(dev)
            pass
    # TODO fix it for  being able to parse radmons everywhere
    # [utils.instance_to_timber(radmon)+':STATUS' for radmon in devices]
    d=ldb.get(timber_names,datetime.now())
    print('not found', not_found_names)
    ret_dict={}
    for key in d.keys():
        ccdb_key=utils.timber_to_instance(key).split(':')[0]
        try:
            delay = datetime.now() - datetime.fromtimestamp(d[key][0][0])
        except IndexError:
            print(' inderror',key) 
        delay_seconds = float(delay.total_seconds())
        ret_dict[ccdb_key]= str('% 6.2f' % delay_seconds)
    for not_found in not_found_names:
        ret_dict[not_found]=0
    return(ret_dict)
    
def get_subscriptions(devices,seconds_subscriptions,timings):
    """
    :param devices: a list of the radmons to scan
    :return: a dictionary with the scanned radmon as keys and the 
                delay in milliseconds between two published data
    """
    d={}
    for radmon in devices:
        d[radmon]=[datetime.now()]
        time_domains=set(timings.values())
    def update_dict(paramName, newVal):
        try:
            d[paramName.split('/')[0]].append(datetime.now())
        except Exception as e:
            print(paramName,e)
    for time_domain in time_domains:
        selector = time_domain + ".USER.ALL"
        japc.setSelector( selector)
        for radmon in devices:
            if timings[radmon]==time_domain:
                #checking only the radmonStatus for now
                japc.subscribeParam(radmon+'/Acquisition#radmonStatus',update_dict,
                                    onException=myexc)#,timingSelectorOvveride=time_domain+'.USER.ALL')
        japc.startSubscriptions()
        time.sleep(seconds_subscriptions)
        japc.stopSubscriptions()
        japc.clearSubscriptions()
    
    for key,val in d.items():
        difference=[m-n for (m,n) in zip(val[1:],val[:1])]
        try: 
            d[key]=(difference[0].microseconds/1000)
        except: 
            d[key]=0
            print(key,d[key]) 
    return d


def get_new_alerts(scanresults,skip_list,old_alerts,not_notify_radmons):
    """gets new alerts from dict"""
    l=[]
    print(not_notify_radmons)
    for radmon in scanresults.keys():
        if radmon.upper() in not_notify_radmons:
            continue
        for variable in scanresults[radmon].keys():
            if (radmon+variable).lower() in old_alerts:
                continue
            if variable.lower() not in skip_list:
                try:
                    if (scanresults[radmon][variable]['warning'][0] in ['H','L']):
                        l.append("{}{}".format(radmon, variable))
                except TypeError:
                    pass
    return l

def write_new_alerts(filename, scanresults):
    "save new alerts file"
    with open(filename,'a+') as f:
        for radmon in scanresults.keys():
            for variable in scanresults[radmon].keys():
                try:
                    if (scanresults[radmon][variable]['warning'][0] in ['H','L']):
                        f.write('{}:{}\n'.format(radmon,variable))
                except TypeError:
                    pass
                
#parser configuration (autodocumented by sphinx)
def parser_generator():
    parser = argparse.ArgumentParser(
            description='Read data from JAPC and Timber and prepare the JSON.',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--JAPCsubscription', dest='JAPC_interval', default = 5, type=int,
                        help='seconds the subscriptions in JAPC are enabled')
    parser.add_argument('--INIfile', dest='INIfile', default = 'resources/allvars_v6.ini', 
                        help='INI file for variables to read')    
    parser.add_argument('--JSONlocal', dest='JSONlocal', default = '/tmp/results.json', 
                        help='absolute path where the JSON is stored locally')
    parser.add_argument('--JSONEOS', dest='JSONEOS', 
                        default = 'eosuser.cern.ch//eos/project/r/radmonit/www/radmon_alarms/radmonv6_alarms.json' ,
                        help='absolute path where the JSON is stored on EOS')
    parser.add_argument('--xlsxEOS', dest='xlsxEOS', 
                        default = 'eosuser.cern.ch//eos/project/r/radmonit/www/radmon_alarms/alerts.xlsx' ,
                        help='absolute path where the xlsx is stored on EOS')

    parser.add_argument('--alertsfile', dest='alertsfile', 
                        default = 'resources/oldalerts.data' ,
                        help='relative path where the old alerts are stored')

    repeat=parser.add_mutually_exclusive_group()
    repeat.add_argument('--only_once', dest='only_once', action='store_true',
                        help='run once and exit')
    repeat.add_argument('--interval', dest='loop_interval', default = 10, type=int,
                        help='Interval in minutes between JSON updates')
    return parser


if __name__ == "__main__":
    def myexc(p,d,e):
        print(p,d,e)
        
    parser = parser_generator()
    args = parser.parse_args()

    #LHC, ALICE, CHARM

    japc = pyjapc.PyJapc( noSet=True )  #safemode
    japc.rbacLogin()
    ldb=pytimber.LoggingDB(source="mdb")   
    filenameINI = args.INIfile
    config = configparser.ConfigParser()
    runtime_config = configparser.ConfigParser(allow_no_value=True)
    EOS_conf = "root://eosuser.cern.ch//eos/project/r/radmonit/www/radmon_alarms/current_conf.ini"
    
    def main_loop():
        try:
            with open('resources/feclist.txt', 'r') as infile:
                instances=json.load(infile)
            subprocess.call(["xrdcp","-f", EOS_conf, 'current_conf.ini'])
            runtime_config.read('current_conf.ini')
            skip_alerts_variable=list(runtime_config['skip_alerts_variables'].keys())
            ignore_radmons_list=list(runtime_config['known_broken_radmons'].keys())
            ignore_instances_list=list(runtime_config['known_off_instances'].keys())
            not_notify_radmons=list(runtime_config['not_notify_radmons'].keys())

            config.read(filenameINI)
            scanresults, scanned_vars = get_monitoring_data(instances,config,ignore_instances_list,ignore_radmons_list)
            print('scanned')
            timings={}
            for radmon in scanresults.keys():
                timings[radmon]=scanresults[radmon]['timing']
            subscriptions = get_subscriptions(list(scanresults.keys()),args.JAPC_interval,timings)
            print('subscribed')
            timber_delays = read_on_timber(list(scanresults.keys()))
            print('delays')
            add_to_scanresults(scanresults,'status_Delay (ms)',subscriptions, 1, 2e3)
            add_to_scanresults(scanresults,'timber_Delay (s)', timber_delays, 0.01,30)
            
            results={}
            #timestamp for the web page
            results['timestamp']=datetime.now().isoformat()
            results['JAPCdata']=scanresults                
            results['excluded_radmons']=ignore_radmons_list
            #write json to local folder, then move to EOS with xroot
            with open(args.JSONlocal,'w+') as f:
                	json.dump(results,f)
            
            #check if there are new alerts since last scan and send mail in case
            oldal=list(runtime_config['specific_skip_variables'].keys())
            newal=get_new_alerts(scanresults,skip_alerts_variable,oldal,not_notify_radmons)
            print('lengths',len(newal),len(oldal))
            
            new_alerts_message= []
            if oldal is not newal:
                for alert in newal: 
                    if alert not in oldal:
                        new_alerts_message.append(alert)            
                
            if new_alerts_message:
                print('writing xlsx')
                write_xlsx(scanresults,results['timestamp'],new_alerts_message,'/tmp/alerts.xlsx')
                root_path="root://"+args.xlsxEOS
                subprocess.call(["xrdcp","-f", '/tmp/alerts.xlsx', \
                             root_path])
                print('sending mail',new_alerts_message)
                utils.send_mail('rcastell@cern.ch','\n'.join(new_alerts_message),
                                subject='New fault in RadmonV6')
                
            #write_new_alerts(args.alertsfile,scanresults)
            #store the data on EOS folder
            root_path="root://"+args.JSONEOS
            
            subprocess.call(["xrdcp","-f", args.JSONlocal, \
                             root_path])
            
        except Exception:
            exc_message=traceback.format_exc()
            utils.send_mail('rcastell@cern.ch', exc_message)
            
    if args.only_once:
        main_loop()
    else:
        while True:
            main_loop()
            time.sleep(args.loop_interval*60) #time.sleep wants seconds
