[![pipeline status](https://gitlab.cern.ch/ecerad/radmon-commissioning/badges/master/pipeline.svg)](https://gitlab.cern.ch/ecerad/radmon-commissioning/commits/master)


This repository stores the scripts used during the 2018 commissioning of v6 RadMons
